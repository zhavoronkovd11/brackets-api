
# Локальная установка
1) `.env.dist` => `.env`
2) `docker-compose.dev.yml` => `docker-compose.yml`
3) `docker-compose up -d --build`
4) `localhost:8000` => `http-api`
5) `localhost:8001` => `grpc-server`

# Документация
http://localhost:8000/docs/index.html

# Тесты
1) Локальный вариант: `cd test/unit && go test`
2) Через докер: `docker-compose -f docker-compose.test.yml run --rm go-test`

# Использование gRPC
1) Можно использовать написанного клиента - 
    - `go run cmd/client-grpc/main.go validate "(())["`
    - `go run cmd/client-grpc/main.go fix "(())["`
