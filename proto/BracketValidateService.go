package bracketservice

import (
	"bitbucket.org/dimarik11/brackets-api/internal/service"
	"context"
)

// Прослойка для gRPC. Внутри используется все тот же сервис
type BracketValidateServiceServer struct {
	service service.BracketService
}

// Создаем новый инстанс
func NewBracketValidateServiceServer(bracketService service.BracketService) *BracketValidateServiceServer {
	return &BracketValidateServiceServer{
		service: bracketService,
	}
}

// Валидация
func (validator *BracketValidateServiceServer) Validate(ctx context.Context, req *ValidateRequest) (*ValidateResponse, error) {
	return &ValidateResponse{IsValid: validator.service.Validate(req.InputString)}, nil
}

// Фикс
func (validator *BracketValidateServiceServer) Fix(ctx context.Context, req *FixRequest) (*FixResponse, error) {
	return &FixResponse{OutputString: validator.service.Fix(req.InputString)}, nil
}
