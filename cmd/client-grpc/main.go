package main

import (
	proto "bitbucket.org/dimarik11/brackets-api/proto"
	"context"
	"google.golang.org/grpc"
	"log"
	"os"
)

// Примитивный клиент для gRPC
func main() {
	conn, _ := grpc.Dial("127.0.0.1:8001", grpc.WithInsecure())

	client := proto.NewBracketValidatorServiceClient(conn)

	countArgs := len(os.Args) - 1
	if countArgs != 2 {
		log.Fatal("expected 2 params, got ", countArgs)
		return
	}
	callFunc := os.Args[1]

	inputString := os.Args[2]

	if callFunc == "validate" {
		validate(client, inputString)
		os.Exit(0)
	}

	if callFunc == "fix" {
		fix(client, inputString)
		os.Exit(0)
	}

	log.Println("operation not found")
	os.Exit(1)
}

// Валидация
func validate(client proto.BracketValidatorServiceClient, inputString string) {
	resp, err := client.Validate(context.Background(), &proto.ValidateRequest{InputString: inputString})
	if err != nil {
		log.Fatalf("could not get answer: %v", err)
	}
	var msg string
	if resp.IsValid == true {
		msg = "Balanced"
	} else {
		msg = "Not balanced"
	}
	log.Println(msg)
}

// Фиксация
func fix(client proto.BracketValidatorServiceClient, inputString string) {
	resp, err := client.Fix(context.Background(), &proto.FixRequest{InputString: inputString})
	if err != nil {
		log.Fatalf("could not get answer: %v", err)
	}

	log.Println(resp.OutputString)
}
