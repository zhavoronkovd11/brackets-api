package main

import (
	_ "bitbucket.org/dimarik11/brackets-api/docs"
	"bitbucket.org/dimarik11/brackets-api/internal/endpoints"
	"bitbucket.org/dimarik11/brackets-api/internal/service"
	"bitbucket.org/dimarik11/brackets-api/internal/transport"
	"log"
	"net/http"
	"os"
)

func main() {
	baseEndpoint := endpoints.MakeEndpoints(service.NewService())

	log.Println("starting api...")
	err := http.ListenAndServe(":"+os.Getenv("HTTP_PORT"), transport.MakeHTTPHandler(baseEndpoint))
	if err != nil {
		log.Fatal(err)
	}
}
