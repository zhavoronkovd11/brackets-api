package main

import (
	"bitbucket.org/dimarik11/brackets-api/internal/service"
	proto "bitbucket.org/dimarik11/brackets-api/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	server := grpc.NewServer()

	instance := proto.NewBracketValidateServiceServer(service.NewService())

	proto.RegisterBracketValidatorServiceServer(server, instance)

	listener, err := net.Listen("tcp", ":"+os.Getenv("TCP_PORT"))
	if err != nil {
		log.Fatal("Unable to create server-grpc listener:", err)
	}

	if err = server.Serve(listener); err != nil {
		log.Fatal("Unable to start server:", err)
	}
}
