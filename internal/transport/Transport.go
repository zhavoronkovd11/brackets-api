package transport

import (
	"bitbucket.org/dimarik11/brackets-api/internal/endpoints"
	"context"
	"encoding/json"
	"errors"
	kitHttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
)

var (
	ErrBadRouting = errors.New("bad routing")
)

// Создаем HTTP хэнддер
func MakeHTTPHandler(
	svcEndpoints endpoints.Endpoints,
) http.Handler {
	r := mux.NewRouter()
	options := []kitHttp.ServerOption{
		kitHttp.ServerErrorEncoder(encodeError),
	}

	r.Methods("GET").Path("/validate/{pattern}").Handler(kitHttp.NewServer(
		svcEndpoints.Validate,
		decodeValidateRequest,
		encodeResponse,
		options...,
	))

	r.Methods("GET").Path("/fix/{pattern}").Handler(kitHttp.NewServer(
		svcEndpoints.Fix,
		decodeFixRequest,
		encodeResponse,
		options...,
	))

	r.Methods("GET").Path("/metrics").Handler(promhttp.Handler())

	r.PathPrefix("/docs").Handler(httpSwagger.WrapHandler)
	return r
}

func decodeFixRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	pattern, ok := vars["pattern"]
	if !ok {
		return nil, ErrBadRouting
	}

	return endpoints.FixReq{InputString: pattern}, nil
}

func decodeValidateRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	pattern, ok := vars["pattern"]
	if !ok {
		return nil, ErrBadRouting
	}

	return endpoints.ValidateReq{InputString: pattern}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if err, ok := response.(error); ok && err != nil {
		encodeError(ctx, err, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(map[string]string{
		"error": err.Error(),
	})
}
