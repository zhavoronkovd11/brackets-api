package endpoints

import (
	"bitbucket.org/dimarik11/brackets-api/internal/service"
	"context"
	"github.com/go-kit/kit/endpoint"
)

// Список ендпоинтов
type Endpoints struct {
	Validate endpoint.Endpoint
	Fix      endpoint.Endpoint
}

// Запрос Validate
type ValidateReq struct {
	InputString string
}

// Ответ Validate
type ValidateRes struct {
	Result bool
}

// Запрос Fix
type FixReq struct {
	InputString string
}

// Ответ Fix
type FixRes struct {
	OutputString string
}

// Создание Endpoint
// @title Brackets API
// @version 1.0
// @description API validate brackets

// @contact.name zhdmirty
// @contact.email zhavoronkovd11@gmail.com

// @license.name MIT

// @BasePath /
func MakeEndpoints(s service.BracketService) Endpoints {
	return Endpoints{
		Fix:      makeFixEndpoint(s),
		Validate: makeValidateEndpoint(s),
	}
}

// makeFixEndpoint godoc
// @Summary Fix brackets
// @Produce json
// @Param pattern path string true "Pattern"
// @Success 200 {object} endpoints.FixRes
// @Router /fix/{pattern} [get]
func makeFixEndpoint(s service.BracketService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(FixReq)
		return FixRes{OutputString: s.Fix(req.InputString)}, nil
	}
}

// Создазим ендпоинт для Validate rpc
// makeValidateEndpoint godoc
// @Summary Validate brackets
// @Produce json
// @Param pattern path string true "Pattern"
// @Success 200 {object} endpoints.ValidateRes
// @Router /validate/{pattern} [get]
func makeValidateEndpoint(s service.BracketService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(ValidateReq)
		return ValidateRes{Result: s.Validate(req.InputString)}, nil
	}
}
