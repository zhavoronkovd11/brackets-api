package service

import (
	"strings"
)

// Контракт для работы со скобками
type BracketService interface {
	Validate(inputStr string) bool
	Fix(inputStr string) string
}

// Реализация скобочного сервиса через слайсы
type sliceBracketService struct {
}

// мапа, хранящая соответствие открытой строке к закрытой
var bracketMap = map[string]string{"(": ")", "[": "]", "{": "}", "<": ">"}

// Создание сервиса
func NewService() BracketService {
	return &sliceBracketService{}
}

// Реализация валидатора
func (service *sliceBracketService) Validate(inputStr string) bool {
	stack := make([]string, 0)
	for _, val := range strings.Split(inputStr, "") {
		if service.isOpenBracket(val) {
			stack = append(stack, val)
		} else if len(stack) > 0 && val == bracketMap[stack[len(stack)-1]] {
			stack = stack[:len(stack)-1]
		} else {
			return false
		}
	}
	return len(stack) == 0
}

// Реализация исправления
func (service *sliceBracketService) Fix(inputStr string) string {
	openStack := make([]item, 0)
	stringSlice := strings.Split(inputStr, "")
	for i, val := range stringSlice {
		if service.isOpenBracket(val) {
			openStack = append(openStack, item{Value: val, Index: i})
		} else if len(openStack) > 0 && val == bracketMap[openStack[len(openStack)-1].Value] {
			openStack = openStack[:len(openStack)-1]
		} else {
			stringSlice[i] = ""
		}
	}

	for _, val := range openStack {
		stringSlice[val.Index] = ""
	}

	return strings.Join(stringSlice, "")
}

// Явялется ли скобка "открывающей"
func (service *sliceBracketService) isOpenBracket(bracket string) bool {
	return bracket == "(" || bracket == "[" || bracket == "{" || bracket == "<"
}

// Просто доп структурка
type item struct {
	Value string
	Index int
}
