package unit

import (
	"bitbucket.org/dimarik11/brackets-api/internal/service"
	"testing"
)

func TestFixBrackets(t *testing.T) {
	serviceBracket := service.NewService()
	res := serviceBracket.Fix("<>(()")

	if res != "<>()" {
		t.Error("Expected <>(), got ", res)
	}
}

func TestValidateNotValidBrackets(t *testing.T) {
	serviceBracket := service.NewService()
	res := serviceBracket.Validate("<>(()")

	if res != false {
		t.Error("Expected false, got ", res)
	}
}

func TestValidateValidBrackets(t *testing.T) {
	serviceBracket := service.NewService()
	res := serviceBracket.Validate("<>(())[[[()]]]")

	if res != true {
		t.Error("Expected true, got ", res)
	}
}
