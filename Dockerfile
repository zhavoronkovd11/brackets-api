# dependency
FROM golang:1.15 as modules
WORKDIR /go/src/app
COPY ./ .
RUN go mod tidy

# building
FROM golang:1.15 as builder
COPY --from=modules /go /go
RUN useradd -u 10001 bracket_api
WORKDIR /go/src/app
ARG GO_FILE
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -o /bin/main ${GO_FILE}

# Final stage: Run the binary
FROM scratch
COPY --from=builder /etc/passwd /etc/passwd
USER bracket_api
COPY --from=builder /bin/main /app
CMD ["/app"]

